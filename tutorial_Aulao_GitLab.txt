#Não vale apagar!!!
#Atualizado por : Adriano Euclides 28/04/21 - telegram: @aeucli

#AULAOGITLAB

Aula01

sysadmin@operacao:~$ git clone https://github.com/badtuxx/DescomplicandoHelm.git


sysadmin@operacao:~$ cd DescomplicandoHelm/
sysadmin@operacao:~/DescomplicandoHelm$ echo Aprendendo GIT com a LinuxTips > giropops.txt  (Alterando arquivo)
sysadmin@operacao:~/DescomplicandoHelm$ git add giropops.txt          (Adicionando as alterações. O arquivo ainda se encontra na maquina local)
sysadmin@operacao:~/DescomplicandoHelm$ git commit -m "Adicionando o arquivo giropops.txt" giropops.txt    (Comitando o arquivo ao diretorio index)

Caso apresente o erro abaixo no momento do commit executar os comando git config com as informações das suas credenciais em seguida comitar novamente o arquivo.

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

sysadmin@operacao:~/DescomplicandoHelm$ git commit -m "Adicionando o arquivo giropops.txt" giropops.txt    (Comitando o arquivo ao diretorio index)

sysadmin@operacao:~/DescomplicandoHelm$ git status

On branch main
Your branch is ahead of 'origin/main' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean


sysadmin@operacao:~/DescomplicandoHelm$ git push  (Aqui sera realizado o push dos arquivos comitados para o servidor remoto. Não precisa especificar o nome do arquivo pois ele já sabe quais arquivos serão comitados. Ou seja, os arquivos que estão em head).

Caso ocorra o erro abaixo ao executar o push, deverá acessar o github com suas credenciais e realizar ao fork do projeto (DescomplicandoHelm).
Em seguida refazer todo os passos anteriormente. Lembrando que antes deverá deletar todo conteudo do diretorio (DescomplicandoHelm) e em seguida fazer o git clone apontando pra url do seu projeto no qual foi realizado o fork. Ex:  git clone https://github.com/aeuclides/DescomplicandoHelm.git 

root@debian:~/teste/DescomplicandoHelm# git push
Username for 'https://github.com': aeuclides
Password for 'https://aeuclides@github.com':
remote: Permission to badtuxx/DescomplicandoHelm.git denied to aeuclides.
fatal: unable to access 'https://github.com/badtuxx/DescomplicandoHelm.git/': The requested URL returned error: 403

Comandos
=========
git clone
git status
gti add
git config
git commit
git push
git log
git log -3
git log --oneline
git pull
git rm


2 - Acessar o Gilabs

https://gitlab.com/

- criar grupo - DevOpsExpert
- marcar public
- Adicionar email de quem fara parte do grupo. Esse grupo terá acesso a este projeto
- Criar um projeto em branco (Create blank project)
- Marcar public
- Marcar Initialize repository with a README
- Clicar em Create
- Add SSH Key
- adiciona o titulo da chave (Nosso caso live Gitlab)
- Se quiser que a chave expire  a data
- Adicione a chave privada no campo Key
- Add Key

- Para criar uma chave publica/privada
acesse o terminal linux e execute
#ssh-keygen -t rsa -b 2048
#ls ~/.ssh   (liste os arquivos criados)
#chmod 400 ~/.ssh/id_rsa (Dê permissao de leitura)
#cat ~/.ssh/id_rsa.pub  (copie a chave ate a ultima letra e cole no campo Key do Gitlab)

Concluida esta etapa retornar a pagina inicial do Gitlab
==========================================

- Clicar no projeto criado
- Clicar em Clone e copia a url do ssh
- No terminal linux criar um diretorio e faz o clone
- git clone (url copiada do ssh acima)

Se a operação for ok resultado será:

# git clone git@gitlab.com:devopsexpert2/desmitificando-o-gitlab.git
Cloning into 'desmitificando-o-gitlab'...
Enter passphrase for key '/root/.ssh/id_rsa':
Enter passphrase for key '/root/.ssh/id_rsa':
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0